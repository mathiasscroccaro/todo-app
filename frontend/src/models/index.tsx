export type Task = {
    id: number;
    title: string;
    created_at: string;
    done: boolean;
    priority: number;
    children_tasks?: Task[] | null;
    parent_task?: number | null;
};

export type TaskState = Task & {
    isExpanded: boolean;
    isEditing: boolean;
    newChildrenTask: boolean;
};

export type PartialTaskState = Omit<Partial<TaskState>, 'id'> & {
    id: number;
};