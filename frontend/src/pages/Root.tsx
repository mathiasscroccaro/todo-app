import React, { useState } from "react";
import {
  BrowserRouter,
  Routes,
  Route,
  Outlet,
  Navigate,
} from "react-router-dom";

import { LoginPage } from "pages/Login";
import { TodoListPage } from "pages/TodoList";
import { ProtectedRoute } from "../components/ProtectedRoute";
import { StoreProvider } from "../components/ProtectedRoute/Store/Provider";

function NotFound() : JSX.Element {
  return <h1>Not Found</h1>;
}

function Root() : JSX.Element {
  document.title = "ToDo App";

  return (
    <BrowserRouter>
      <StoreProvider>
        <Routes>
          <Route path="/" element={<Navigate to="/login" />} />
          <Route path="login" element={<LoginPage />} />
          <Route path="/*" element={<ProtectedRoute />}>
            <Route path="todo-list" element={<TodoListPage />} />
            <Route path="*" element={<NotFound />} />
          </Route>
        </Routes>
      </StoreProvider>
    </BrowserRouter>
  );
}

export default Root;
