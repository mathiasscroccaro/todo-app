import React from 'react';
import { StoreContext } from './Context';
import { useStorage } from './Hooks/useStorage';

export function StoreProvider(props : { children: JSX.Element }) {
  const [token, setToken] = useStorage('token');

  return (
    <StoreContext.Provider
      value={{
        token,
        setToken,
      }}
    >
      {props.children}
    </StoreContext.Provider>
  )
}