import { createContext } from 'react';

type Token = {
  token: string | null;
  setToken: any
}

export const StoreContext = createContext<Token>({
  token: null,
  setToken: () => {},
});