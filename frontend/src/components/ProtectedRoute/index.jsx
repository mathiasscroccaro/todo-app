import React, { useContext } from "react";
import { Outlet, Navigate } from "react-router-dom";
import { StoreContext } from "./Store/Context";

export function ProtectedRoute() {
  const { token } = useContext(StoreContext);
  return token ? <Outlet /> : <Navigate to="/login" />;
}