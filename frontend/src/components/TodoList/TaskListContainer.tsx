import React, { useState, useEffect, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { StoreContext } from "components/ProtectedRoute/Store/Context";
import { TaskList } from "./TaskList";
import { TaskState } from "models";
import axios from "axios";

const TASKS_URL = "http://localhost:8000/tasks/";

export function TaskListContainer() : JSX.Element | null {
  const [tasks, setTasks] = useState([]);
  const { token } = useContext(StoreContext);
  
  //const _tasks: TaskState[] = useSelector((tasksList: TaskState[]) => tasksList);
  //const dispatch = useDispatch();

  async function fetchTasksData () : Promise<void> {
    const response = await axios.get(TASKS_URL, { headers: { 'Authorization': `Token ${token}` }});
    const data = response.data;
    console.log(data);
    // dispatch({
    //   type: "LOAD_INIT_DATA",
    //   initState: data
    // });
    setTasks(data);
  }

  useEffect(() => {
    fetchTasksData();
  },[]);

  return Boolean(tasks.length > 0) ? <TaskList tasks={tasks} /> : null;
};

export default TaskListContainer;
