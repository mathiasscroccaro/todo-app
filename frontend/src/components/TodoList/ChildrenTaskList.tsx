import React, { useEffect, useState, useRef } from "react";

import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import EditIcon from "@mui/icons-material/Edit";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import SaveIcon from '@mui/icons-material/Save';
import DeleteIcon from "@mui/icons-material/Delete";

import List from "@mui/material/List";
import TextField from "@mui/material/TextField";
import ListItem from "@mui/material/ListItem";

import {Task} from "models";

const ENTER_KEY = 13;

export function ChildrenTaskItem(props : Task & { key: number }) {
  const [done, setDone] = useState(false);
  const [edit, setEdit] = useState(false);
  const [taskTitle, setTaskTitle] = useState(props.title);

  let editableText;
  let editableCheck;
  let editableEdit;

  if (edit) {
    editableText = (
      <TextField
        label="Edit me:"
        id="outlined-size-small"
        defaultValue={taskTitle}
        size="small"
        onChange={(e) => setTaskTitle(e.target.value)}
        inputRef={input => input && input.focus()}
        onKeyDown={(e) => e.keyCode==ENTER_KEY ? handleEditClick(props.id) : null}
      />
    );
  } else if (done) {
    editableText = (
      <Typography sx={{ textDecorationLine: "line-through" }}>
        {taskTitle}
      </Typography>
    );
  } else {
    editableText = <Typography>{taskTitle}</Typography>;
  }
  editableCheck = done ? <CheckBoxIcon /> : <CheckBoxOutlineBlankIcon />;
  editableEdit = edit ? <SaveIcon /> : <EditIcon />;

  function handleDeleteClick(id: number) {
    console.log(`Deleting task id: ${id}`);
  }

  function handleEditClick(id: number) {
    edit ? console.log(`Saving Task ${id} as ${taskTitle}`) : null;
    !taskTitle && edit ? handleDeleteClick(id) : null;
    setEdit(!edit);
  }

  function handleCheckClick() {
    edit ? null : setDone(!done);
  }

  return (
    <ListItem>
      <IconButton onClick={() => handleCheckClick()}>
        {editableCheck}
      </IconButton>
      <IconButton onClick={() => handleEditClick(props.id)}>
        {editableEdit}
      </IconButton>
      <IconButton onClick={() => handleDeleteClick(props.id)}>
        <DeleteIcon />
      </IconButton>
      {editableText}
    </ListItem>
  );
}

export default function ChildrenTaskList(props: { tasks: Task[], parent_id: number }) {
  return (
    <List sx={{ pl: 3 }} disablePadding>
      {props.tasks.map((task: Task, key: number) => (
        <ChildrenTaskItem key={key} {...task} />
      ))}
    </List>
  );
}