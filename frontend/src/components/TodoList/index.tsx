import React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import TaskListContainer from "./TaskListContainer";
import { Provider } from "react-redux";
import { store } from "components/Store/index"

const theme = createTheme();

export function TodoList(): JSX.Element {
  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Provider store={store}>
          <TaskListContainer />
        </Provider>
      </Container>
    </ThemeProvider>
  );
}
