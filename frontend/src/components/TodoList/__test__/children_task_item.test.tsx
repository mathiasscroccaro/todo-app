import { render, screen, cleanup, fireEvent } from '@testing-library/react';
import { ChildrenTaskItem } from "../ChildrenTaskList";
import { Task } from "models"


afterEach(cleanup);

const sampleTask: Task = {
    created_at: new Date().toISOString(),
    done: false,
    priority: 0,
    title: "Go to the market",
    id: 1
}

describe("Testing edit button", () => {
    beforeEach(() => {
        render(<ChildrenTaskItem key={0} {...sampleTask} />)
    });

    it("Should modify the title form", () => {
        /**
         * GIVEN: Child task with a pre-defined title
         * WHEN: The edit button is clicked
         * AND: A new text is inserted and the save button is clicked
         * THEN: The new title is verified in the child task
         */

        const taskTitleBeforeClick = screen.getByText("Go to the market");
        expect(taskTitleBeforeClick).toBeTruthy();

        const editIcon = screen.getByTestId('EditIcon');

        fireEvent.click(editIcon);

        const textBoxAfterClick = screen.getByRole('textbox');
        expect(textBoxAfterClick).toBeTruthy();

        fireEvent.change(textBoxAfterClick, {target: {value: 'New title'}});  
        const saveIcon = screen.getByTestId('SaveIcon');
        fireEvent.click(saveIcon);
        
        const taskTitleAfterClick = screen.getByText('New title');
        expect(taskTitleAfterClick).toBeTruthy();
    })
});

//const deleteIcon = screen.getByTestId('DeleteIcon');
//const checkBoxOutlineBlankIcon = screen.getByTestId('CheckBoxOutlineBlankIcon');
