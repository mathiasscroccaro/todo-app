import React, { useEffect, useState, useRef } from "react";

import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import IconButton from "@mui/material/IconButton";
import EditIcon from "@mui/icons-material/Edit";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import SaveIcon from "@mui/icons-material/Save";
import { red } from "@mui/material/colors";
import DeleteIcon from "@mui/icons-material/Delete";
import CardContent from "@mui/material/CardContent";

import List from "@mui/material/List";
import Input from "@mui/material/Input";
import TextField from "@mui/material/TextField";
import ListItem from "@mui/material/ListItem";
import Collapse from "@mui/material/Collapse";

import ArrowRightIcon from "@mui/icons-material/ArrowRight";
import ControlPointIcon from "@mui/icons-material/ControlPoint";
import HorizontalRuleIcon from "@mui/icons-material/HorizontalRule";

import ChildrenTaskList from "./ChildrenTaskList";

import { Task } from 'models';

const ENTER_KEY = 13;

const initialState = {
  expanded: false,
  isAddingChildrenTaks: false,
};

function TaskItem(props : Task & { key: number }) : JSX.Element {
  const [expanded, setExpanded] = useState(false);
  const [isAddingChildrenTask, setAddingChildrenTask] = useState(false);
  const [edit, setEdit] = useState(false);
  const [taskTitle, setTaskTitle] = useState(props.title);

  let editableExpandIcon;
  let editableExpand;
  let editableText;
  let editableEdit;

  editableExpandIcon = props.children_tasks ? (
    <IconButton onClick={() => setExpanded(!expanded)}>
      <ExpandMoreIcon />
    </IconButton>
  ) : (
    <IconButton>
      <HorizontalRuleIcon />
    </IconButton>
  );

  editableExpand = props.children_tasks ? (
    <ChildrenTaskList
      tasks={props.children_tasks}
      parent_id={props.id}
    />
  ) : null;

  editableText = edit ? (
    <TextField
      label="Edit me:"
      id="outlined-size-small"
      defaultValue={taskTitle}
      size="small"
      onChange={(e) => setTaskTitle(e.target.value)}
      inputRef={(input) => input && input.focus()}
      onKeyDown={(e) =>
        e.keyCode == ENTER_KEY ? handleEditClick(props.id) : null
      }
    />
  ) : (
    <Typography>{taskTitle}</Typography>
  );

  editableEdit = edit ? <SaveIcon /> : <EditIcon />;

  function handleAddingChildrenTaskClick(id: number) {
    setAddingChildrenTask(true);
    setExpanded(true);
  }

  function handleEditClick(id: number) {
    edit ? console.log(`Saving Task ${id} as ${taskTitle}`) : null;
    !taskTitle && edit ? handleDeleteClick(id) : null;
    setEdit(!edit);
  }

  function handleDeleteClick(id: number) {
    console.log(`Deleting task id: ${id}`);
  }

  const handleExpandClick = () => {
    setExpanded((expanded) => !expanded);
    setAddingChildrenTask((adding) => !adding);
  };

  return (
    <>
      <List>
        <ListItem>
          <IconButton onClick={() => handleEditClick(props.id)}>
            {editableEdit}
          </IconButton>
          <IconButton>
            <DeleteIcon />
          </IconButton>
          {editableText}
          <IconButton
            sx={{ ml: "auto" }}
          >
            <ControlPointIcon />
          </IconButton>
          {editableExpandIcon}
        </ListItem>
      </List>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        {editableExpand}
      </Collapse>
    </>
  );
}

export function TaskList(props : { tasks: Task[] }) : JSX.Element {
  return (
    <>
      {props.tasks.map((task, key) => (
        <TaskItem
          key={key}
          {...task}
        />
      ))}
    </>
  );
}
