import {
  addTasks,
  updateTask,
  deleteTask,
  addChildTask,
  updateChildTask,
  deleteChildTask,
  tasksSlice
} from "components/Store/tasksSlice";
import { TaskState } from "models";


const reducer = tasksSlice.reducer;

let mockedInitialTaskState: TaskState = {
  title: "dummy task",
  created_at: "01/01/2022",
  done: true,
  id: 1,
  priority: 0,
  isEditing: false,
  isExpanded: false,
  newChildrenTask: false,
  children_tasks: null,
  parent_task: null,
};

describe("Testing operations with the reducer", () => {
  it("Should create a parent task", () => {
    /*
        GIVEN: An application with one parent task 
        WHEN: Is trigged the creation action of a new task
        THEN: Is observed two tasks in the state of the application
    */
    const previousState = [mockedInitialTaskState, mockedInitialTaskState];

    const payload = [mockedInitialTaskState];

    const expected = [...previousState, ...payload];

    expect(reducer(previousState, addTasks(payload))).toEqual(expected);
  });

  it("Should delete a parent task", () => {
    /*
        GIVEN: An application with two different parent task
        WHEN: Is trigged the delete action of one of the tasks
        THEN: Is observed the selected task is deleted
    */
    const idToBeDeleted = 3;

    const previousState = [
      mockedInitialTaskState,
      {
        ...mockedInitialTaskState,
        id: idToBeDeleted,
      },
    ];

    const payload = idToBeDeleted;

    let expected = [mockedInitialTaskState];

    expect(reducer(previousState, deleteTask(payload))).toEqual(expected);
  });

  it("Should update a parent task", () => {
    /*
        GIVEN: An application with one parent task
        WHEN: Is trigged the update action of the task
        THEN: Is observed the selected task is updated
    */

    const idToBeModified = 3;
    const newTitle = "This is my new title";

    const previousState = [
      mockedInitialTaskState,
      {
        ...mockedInitialTaskState,
        id: idToBeModified,
      },
    ];

    const payload = {
      id: idToBeModified,
      title: newTitle,
    };

    const expected = [mockedInitialTaskState, {...mockedInitialTaskState, id: idToBeModified, title: newTitle}];

    expect(reducer(previousState, updateTask(payload))).toEqual(expected);
  });

  it("Should create a child task", () => {
    /*
        GIVEN: An application with one parent task 
        WHEN: Is trigged the creation action of a new child task
        THEN: Is observed one child task in the state of the application
    */
    const previousState = [
      {
        ...mockedInitialTaskState,
      },
    ];

    const payload = {
      ...mockedInitialTaskState,
      id: 2,
      parent_task: 1,
    };

    let expected = [
      {
        ...mockedInitialTaskState,
        children_tasks: [{...payload}],
      },
    ];

    expect(reducer(previousState, addChildTask(payload))).toEqual(expected);
  });

  it("Should not create a child task", () => {
    /*
        GIVEN: An application with one parent task
        WHEN: Is trigged the creation action of a new child task with wrong parent_task id
        THEN: Is observed none child tasks
    */
    const previousState = [
      {
        ...mockedInitialTaskState,
      },
    ];

    const payload = {
      ...mockedInitialTaskState,
      id: 2,
      parent_task: 2,
    };

    let expected = [
      {
        ...mockedInitialTaskState
      },
    ];

    expect(reducer(previousState, addChildTask(payload))).toEqual(expected);
  });

  it("Should delete a child task", () => {
    /*
        GIVEN: An application with one parent and two children tasks
        WHEN: Is trigged the delete action of one of the child task        
        THEN: Is observed the selected child task is deleted, remaining
        one last child task
    */
    const idToBeDeleted = 2;

    const previousState = [
      {
        ...mockedInitialTaskState,
        children_tasks: [
          {
            ...mockedInitialTaskState,
            id: idToBeDeleted,
            parent_task: 1,
          }
        ]
      }
    ]

    const payload = idToBeDeleted;

    let expected = [
      {
        ...mockedInitialTaskState,
        children_tasks: [],
      },
    ];

    expect(reducer(previousState, deleteChildTask(payload))).toEqual(expected);
  });

  it("Should update a child task", () => {
    /*
        GIVEN: An application with one parent and one child task 
        WHEN: Is trigged the update action of the child task
        THEN: Is observed the selected child task is updated
    */
    const idToBeUpdated = 2;
    const newTitle = 'This is my new title';

    const previousState = [
      {
        ...mockedInitialTaskState,
        children_tasks: [
          {
            ...mockedInitialTaskState,
            id: 2,
            parent_task: 1,
          }
        ]
      }
    ]

    const payload = {
      id: idToBeUpdated,
      title: newTitle,
      parent_task: 1
    }

    const expected = [
      {
        ...mockedInitialTaskState,
        children_tasks: [
          {
            ...mockedInitialTaskState,
            id: idToBeUpdated,
            parent_task: 1,
            title: newTitle
          }
        ]
      }
    ]

    expect(reducer(previousState, updateChildTask(payload))).toEqual(expected);
  });

  it("Should not update a child task", () => {
    /*
        GIVEN: An application with one parent and one child task 
        WHEN: Is trigged the update action of the wrong child task
        THEN: Is observed the selected child task is not updated
    */
    const idToBeUpdated = 2;
    const newTitle = 'This is my new title';

    const previousState = [
      {
        ...mockedInitialTaskState,
        children_tasks: [
          {
            ...mockedInitialTaskState,
            id: 3,
            parent_task: 1,
          }
        ]
      }
    ]

    const payload = {
      id: idToBeUpdated,
      parent_task: 1,
      title: newTitle
    }

    const expected = [
      {
        ...mockedInitialTaskState,
        children_tasks: [
          {
            ...mockedInitialTaskState,
            id: 3,
            parent_task: 1,
          }
        ]
      }
    ]

    expect(reducer(previousState, updateChildTask(payload))).toEqual(expected);
  });
});
