import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import type { TaskState, PartialTaskState } from "models";


const initialState: TaskState[] = [];

export const tasksSlice = createSlice({
  name: "tasksState",
  initialState,
  reducers: {
    addTasks: (state, action: PayloadAction<TaskState[]>) => {
      return [...state, ...action.payload];
    },
    updateTask: (state, action: PayloadAction<PartialTaskState>) => {
      return state.map((taskState) => 
        taskState.id === action.payload.id ? { ...taskState, ...action.payload } : taskState
      );
    },
    deleteTask: (state, action: PayloadAction<number>) => {
      return state.filter((taskState) => taskState.id !== action.payload);
    },
    addChildTask: (state, action: PayloadAction<TaskState>) => {
      return state.map((taskState) => {
        if (taskState.id === action.payload.parent_task) {
          const newChildrenTasks = taskState.children_tasks
          ? [...taskState.children_tasks, action.payload]
          : [action.payload];
          return { ...taskState, children_tasks: newChildrenTasks };
        }
        return taskState;
      });
    },
    updateChildTask: (state, action: PayloadAction<PartialTaskState>) => {
      return state.map(taskState => {
        if (taskState.id === action.payload.parent_task) {
          const newChildrenTasks = taskState.children_tasks
          ? taskState.children_tasks.map(childTask => {
            if (childTask.id === action.payload.id)
              return {...childTask, ...action.payload};
            else
              return childTask;
          })
          : [];
          return { ...taskState, children_tasks: newChildrenTasks }
        }
        return taskState;
      })
    },
    deleteChildTask: (state, action: PayloadAction<number>) => {
      return state.map((taskState) => {
        const newChildrenTasks = taskState.children_tasks
        ? taskState.children_tasks.filter(childTask => childTask.id !== action.payload)
        : [];
        return { ...taskState, children_tasks: newChildrenTasks }
      });
    },
  },
});

export const {
  addTasks,
  updateTask,
  deleteTask,
  addChildTask,
  updateChildTask,
  deleteChildTask,
} = tasksSlice.actions;
