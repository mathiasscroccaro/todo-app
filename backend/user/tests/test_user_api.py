from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from rest_framework.test import APIClient
from rest_framework import status
from rest_framework.authtoken.models import Token


TOKEN_URL = reverse('user:token')


def create_user(**params):
    return get_user_model().objects.create_user(**params)


class PrivateUserApiTests(TestCase):
    def setUp(self):
        self.username = 'test@example.com'
        self.password = 'testpass123'

        self.user = create_user(
            username=self.username,
            password=self.password,
        )
        self.client = APIClient()

    def test_login(self):
        payload = {
            'username': self.username,
            'password': self.password,
        }
        res = self.client.post(TOKEN_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(res.data.get('token', None))
        self.client.force_authenticate(token=res.data['token'])
        token = Token.objects.get(user__username=self.username)
        self.assertEqual(res.data['token'], token.key)
    
    def test_login_with_wrong_credentials(self):
        payload = {
            'username': self.username,
            'password': 'wrongpass',
        }
        res = self.client.post(TOKEN_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
