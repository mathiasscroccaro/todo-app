# If your DB models were changed

```bash
docker-compose run --rm todo_backend python3 manage.py makemigrations todolist
docker-compose run --rm todo_backend python3 manage.py migrate
```

# How to create a new app in the application

`docker-compose run --rm todo_backend python3 manage.py startapp <appname>`

# How to test

`docker-compose run --rm todo_backend python3 manage.py test`

# How to run

`docker-compose up`
