from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APIClient

from task.models import (
    Task
)

from datetime import datetime

from task.serializers import (
    TaskSerializer
)


TASKS_URL = reverse('task:task-list')


def detail_url(task_id):
    return reverse('task:task-detail', args=[task_id])


def create_task(author, **params):
    defaults = {
        'title': 'Sample task title',
        'created_at': datetime.now(),
        'priority': 0,
        'done': False,
    }
    defaults.update(params)

    task = Task.objects.create(author=author, **defaults)
    return task


def create_user(username='user_example', password='test123', **params):
    return get_user_model().objects.create_user(
        username=username,
        password=password,
        **params
    )


class PublicTaskAPITests(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_auth_required(self):
        res = self.client.get(TASKS_URL)

        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateTaskAPITests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = create_user()
        self.client.force_authenticate(self.user)

    def test_retrieve_task(self):
        create_task(author=self.user)
        create_task(author=self.user)

        res = self.client.get(TASKS_URL)

        tasks = Task.objects.all().order_by('-created_at')
        serializer = TaskSerializer(tasks, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)
    
    def test_task_list_limited_to_user(self):
        other_user = create_user(username='other_example')
        create_task(author=other_user)
        create_task(author=self.user)

        res = self.client.get(TASKS_URL)

        tasks = Task.objects.filter(author=self.user)
        serializer = TaskSerializer(tasks, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_get_task_detail(self):
        task = create_task(author=self.user)

        url = detail_url(task.id)
        res = self.client.get(url)

        serializer = TaskSerializer(task)
        self.assertEqual(res.data, serializer.data)
    
    def test_create_task(self):
        payload = {
            'title': 'Sample task',
            'priority': 0,
            'done': False
        }
        res = self.client.post(TASKS_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        task = Task.objects.get(id=res.data['id'])
        for k, v in payload.items():
            self.assertEqual(getattr(task, k), v)
        self.assertEqual(task.author, self.user)

    def test_partial_update(self):
        original_title = 'Sample task'
        task = create_task(
            author=self.user,
            title=original_title,
        )

        payload = {'title': 'New task title'}
        url = detail_url(task.id)
        res = self.client.patch(url, payload)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        task.refresh_from_db()
        self.assertEqual(task.title, payload['title'])
        self.assertEqual(task.author, self.user)
    
    def test_full_update(self):
        task = create_task(
            author=self.user,
            title='Sample task title',
            priority=0,
            done=False,
        )

        payload = {
            'title': 'New task title',
            'priority': 1,
            'done': True,
        }
        url = detail_url(task.id)
        res = self.client.put(url, payload)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        task.refresh_from_db()
        for k, v in payload.items():
            self.assertEqual(getattr(task, k), v)
        self.assertEqual(task.author, self.user)

    def test_update_user_returns_error(self):
        new_user = create_user(username='user2@example.com', password='test123')
        task = create_task(author=self.user)

        payload = {'author': new_user.id}
        url = detail_url(task.id)
        self.client.patch(url, payload)

        task.refresh_from_db()
        self.assertEqual(task.author, self.user)

    def test_delete_task(self):
        task = create_task(author=self.user)

        url = detail_url(task.id)
        res = self.client.delete(url)

        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Task.objects.filter(id=task.id).exists())

    def test_task_other_users_task_error(self):
        new_user = create_user(username='user2@example.com', password='test123')
        task = create_task(author=new_user)

        url = detail_url(task.id)
        res = self.client.delete(url)

        self.assertEqual(res.status_code, status.HTTP_404_NOT_FOUND)
        self.assertTrue(Task.objects.filter(id=task.id).exists())

    def test_list_nested_tasks(self):
        create_task(
            author=self.user,
            title='Sample test title',
            priority=1,
        )
        parent_task = create_task(
            author=self.user,
            title='Sample test title with children',
            priority=0,
        )
        child_task = create_task(
            author=self.user,
            title='Sample child test title',
            parent_task=parent_task
        )
        
        url = detail_url(parent_task.id)
        res = self.client.get(url)
        
        serializer = TaskSerializer(child_task)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data['children_tasks']), 1)
        self.assertEqual(dict(res.data['children_tasks'][0]), serializer.data)
    
    def test_update_nested_task(self):
        original_parent_task = create_task(
            author=self.user,
            title='Sample test title',
            priority=1,
        )
        new_parent_task = create_task(
            author=self.user,
            title='Sample test title with children',
            priority=0,
        )
        child_task = create_task(
            author=self.user,
            title='Sample child test title',
            parent_task=original_parent_task
        )
        
        payload = {'parent_task': new_parent_task.id}
        url = detail_url(child_task.id)
        res = self.client.patch(url, payload)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data['parent_task'], new_parent_task.id)
    
    def test_delete_nested_task(self):
        parent_task = create_task(
            author=self.user,
            title='Sample test title',
            priority=1,
        )
        child_task = create_task(
            author=self.user,
            title='Sample child test title',
            parent_task=parent_task
        )

        url = detail_url(child_task.id)
        res_child = self.client.delete(url)
        url = detail_url(parent_task.id)
        res_parent = self.client.get(url)

        self.assertEqual(res_child.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(res_parent.status_code, status.HTTP_200_OK)
        self.assertIsNone(res_parent.get('children_tasks', None))