

from rest_framework import serializers

from task.models import Task


class TaskSerializer(serializers.ModelSerializer):
    children_tasks = serializers.SerializerMethodField()

    class Meta:
        model = Task
        fields = '__all__'
        read_only_fields = ['id', 'author']
    
    def to_representation(self, instance):
        serialized_tasks = super().to_representation(instance)
        if not serialized_tasks['children_tasks']:
            serialized_tasks.pop('children_tasks')
        return serialized_tasks
    
    def get_children_tasks(self, instance):
        if instance.children_tasks:
            serializer = TaskSerializer(instance.children_tasks, many=True)
            return serializer.data
        return []
